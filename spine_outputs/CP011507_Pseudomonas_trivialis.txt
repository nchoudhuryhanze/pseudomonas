Spine version: 0.3.2
inputs: --pctcore 0 --refs 1,2 --maxdist 10 --pctid 85 --minout 10

gen_#	gen_name	gen_size	source		total_bp	gc_%	num_segs	min_seg	max_seg	avg_leng	median_leng	num_cds
1	p11		8195988		accessory	3348020	61.7454	2026	10	75954	1652.53	1245	0
1	p11		8195988		core		4847690	60.3316	793	66	46668	6113.10	3937	0
2	ncbi_source	6452803		accessory	1542775	57.9408	882	10	71294	1749.18	190.5	0
2	ncbi_source	6452803		core		4909917	60.5120	902	79	72135	5443.37	3232.5	0
-	-	-	backbone			4847690	60.3316	793	66	46668	6113.10	3937	0
