Spine version: 0.3.2
inputs: --pctcore 0 --refs 1,2 --maxdist 10 --pctid 85 --minout 10

gen_#	gen_name	gen_size	source	total_bp	gc_%	num_segs	min_seg	max_seg	avg_leng	median_leng	num_cds
1	r96	6447297	accessory	6433828	59.0666	81	27	658047	79429.98	29737	0
1	r96	6447297	core	13469	53.4858	8	100	6933	1683.62	182	0
2	ncbi	15918	accessory	3670	62.3706	1	3670	3670	3670.00	3670	0
2	ncbi	15918	core	12248	55.0294	1	12248	12248	12248.00	12248	0
-	-	-	backbone	13469	53.4858	8	100	6933	1683.62	182	0
