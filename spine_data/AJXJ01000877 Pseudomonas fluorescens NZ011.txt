gen_#	gen_name	gen_size	source	    total_bp	gc_%	num_segs	min_seg	max_seg	avg_leng	median_leng	num_cds
1	    Sample_P42	7210656	    accessory	2992843	    59.3753	1730	    10  	81862	1729.97 	1121.5  	0
1	    Sample_P42	7210656	    core	    4217677	    59.8427	967	        38  	39697	4361.61 	2954    	0
2	    Sample_P56	11070922	accessory	6211664	    58.8067	3042	    10  	70355	2041.97 	1356    	0
2	    Sample_P56	11070922	core	    4858656	    59.8216	1530	    23	    39565	3175.59 	1707	    0
3	    Sample_P72	6038783	    accessory	1834542	    57.1172	981	        11  	109769	1870.07 	244	        0
3	    Sample_P72	6038783	    core	    4204171	    59.7964	977	        92	    39697	4303.14 	2951        0
-	    -	        -	        backbone	4217677	    59.8427	967	        38	    39697	4361.61 	2954    	0
